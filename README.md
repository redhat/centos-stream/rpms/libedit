# libedit

[Libedit](https://www.thrysoee.dk/editline/) is an autotool- and libtoolized
port of the
[NetBSD Editline library](http://cvsweb.netbsd.org/bsdweb.cgi/src/lib/libedit/?sortby=date#dirlist).
It provides generic line editing, history, and tokenization functions, similar
to those found in
[GNU Readline](https://tiswww.case.edu/php/chet/readline/rltop.html).
